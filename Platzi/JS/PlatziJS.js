var quique = {
    name: 'Luis Enrique',
    apellido: 'Serralde',
    age: 23
} 

var cindirela = {
    name: 'Cinthia',
    apellido: 'Gallardo',
    age: 23
} 

var alex = {
    name: 'Alexis',
    apellido: 'Gallardo',
    age: 17
} 

// function esMayorDeEdad(persona){
//     return persona.age >= 18;
// }
const esMayorDeEdad = persona => persona.age >= 18

function mayorDeEdad(persona){
    // if(persona.age > 18){
    if(esMayorDeEdad(persona)){
        console.log(`${persona.name} es mayor de edad, tiene ${persona.age}`);
    } else {
        console.log(`${persona.name} no es mayor de edad, tiene ${persona.age}`);
    }
}

function permitirAcceso(persona){
    if(!esMayorDeEdad(persona)){
        console.log('ACCESO DENEGADO')
    }
}
