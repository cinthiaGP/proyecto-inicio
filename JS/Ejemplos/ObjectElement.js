var div = document.querySelector('.info');

div.textContent = 'Hola a todos'; //Añade texto dentro del elemento
console.log(div.textContent); //Se muestra en consola

div.innerHTML = '<strong>Importante</strong>';
console.log(div.innerHTML); //Se muestra en consola
console.log(div.textContent); //Se muestra en consola

//CLONAR UN DIV
// var div1 = document.createElement('div1');
// div1.textContent = 'Esto es un div insertado con JS.';
// var app = document.querySelector('#app'); //<div id="app"></div>
// app.appendChild(div1);

// var div2 = div1.cloneNode();
// div2.textContent = 'Elemento 2';
// console.log(div2.textContent);