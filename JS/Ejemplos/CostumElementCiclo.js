class MiMensaje extends HTMLElement{
    constructor(){
        super();
        this.addEventListener('click', function(e){
            alert('Click en mensaje');
        });
        console.log('constructor: Cuando el elemento es creado');
    }

    //Propiedad para escuchar los atributos definidos en el array usando
    //attributeChangedCallback
    static get observedAttributes(){
        return ['msj', 'casi-vsible'];
        //return ['msj'];
    }

    connectedCallback(){
        console.log('connectedCallback: Cuando el elemento es insertado en el elemento');
    }

    disconnectedCallback(){
        alert('disconnected: Cuando el elemento es elimindado del documento')
    }

    adoptedCallback(){
        alert('adoptedCallback: Cuando el elemento es adoptado por otro documento');
    }

    //Cuando el attr es modificado, solo llamado en atributos reservados definidos en las prop observedAttributes
    attributeChangedCallback (attrName, oldVal, newVal){
        console.log('attributeChangedCallback: Cuando cambia un atributo');

        if(attrName === 'msj'){
            this.pintarMensaje(newVal);
        }
        if(attrName === 'casi.visible'){
            this.setCasiVisible();
        }
    }

    pintarMensaje(msj){
        this.innerHTML = msj;
    }


    //accceso a los atributos del DOM 
    //Propiedad msj sincronizada con el atributos msj
    get msj(){
        return this.getAttribute('msj');
    }

    set msj(val){
        this.setAttribute('msj', val);
    }

    //Propiedad casi visible sincronizada con el atributo 'casi-visible'
    get casiVisible(){
        return this.hasAttribute('casi-visible');
    }

    set casiVisible(value){
        if(value){
            this.setAttribute('casi-visible', '')
        } else {
            this.removeAttribute('casi-visible');
        }
    }

    //define la opacidad del elemento basado en la prop casiVisible
    setCasiVisible(){
        if(this.casiVisible){
            this.style.opacity = 0.1;
        } else {
            this.style.opacity = 1;
        }
    }
}

customElements.define('mi-mensaje', MiMensaje);

let miMensaje = document.createElement('mi-mensaje')
miMensaje.msj = 'Otro mensaje';
document.body.appendChild(miMensaje);

let tercerMensaje = new MiMensaje();
tercerMensaje.msj ='Tercer mensaje';
document.body.appendChild(tercerMensaje);