class MiBotonExtendido extends HTMLButtonElement{
    constructor(){
        super();
        this.addEventListener('click', (e)=>{
            console.log("evento click: " + this.innerHTML);
            alert('Mi boton extendido');
        });
    }

    static get ceName(){
        return 'mi-boton-extendido';
    }

    get is(){
        return this.getAttribute('is');
    }

    set is (value){
        this.setAttribute('is', value || this.ceName);
    }
}

customElements.define(MiBotonExtendido.ceName, MiBotonExtendido, {extends: 'button'});


// Creando un elemento usando la API del DOM
// const miBotonExtendido2 = document.createElement('button', {is: 'mi-boton-entendido'});
const miBotonExtendido2 = document.createElement('button', {is: MiBotonExtendido.ceName});

miBotonExtendido2.textContent = 'Soy un mi-boton-extendido';
document.body.appendChild(miBotonExtendido2);

const miBotonExtendido3 = document.createElement('button', {is: MiBotonExtendido.ceName});
miBotonExtendido3.textContent = 'Hola btn-3';
document.querySelector('#acontainer').appendChild(miBotonExtendido3);
