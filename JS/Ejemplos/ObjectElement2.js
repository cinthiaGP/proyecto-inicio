var div = document.createElement('div');
div.textContent = 'Esto es un div insertado con JS.';

var app = document.querySelector('#app'); //<div id="app"></div>
app.appendChild(div);

//FORMA NO ADECUADA DE AGREGAR POR INNERHTML//
// var fieldset = document.createElement('fieldset');
// fieldset.innerHTML = '<label>Hola</label><div>inicial</div>';
// app.appendChild(fieldset);

//FORMA CORRECTA DE CREAR ELEMENTOS EN HTML//
var fieldset = document.createElement('fieldset');

var label = document.createElement('label');
label.textContent = 'Hola';
fieldset.appendChild(label);

var div3 = document.createElement('div');
div3.textContent = 'inicial';
fieldset.appendChild(div3);
app.appendChild(fieldset);