class Animal {
    constructor(n){
        this.nombre = n;
    }

    get nombre(){
        console.log('Metodo GET');
        return 'Sr. ' + this._nombre; 
        //GET Y SET misma propidad pero con _ para no confundirse 
    }

    set nombre(n){
        console.log('Metodo SET');
        this._nombre = n.trim(); //Metodo de la clase String - Elimina espacios al inicio y el final
    }

    /*get nombre(){
        console.log('Metodo GET');
        console.log('No lo puedes ver');
        return ""; 
    }

    set nombre(n){
        console.log('Metodo SET');
        console.log('No lo puedes modificar');
        //this._nombre = n.trim();
    }*/

    hablar(){
        return "Cuak";
    }

    quienSoy(){
        return "Hola, soy " + this.nombre;
    }
}

var pato = new Animal('Donald');
// pato.nombre;
console.log(pato.nombre);
//Modificación de nombre
// pato.nombre = "Luca";
/*pato.nombre = "Luca";
console.log(pato.nombre);*/
// pato.nombre;
