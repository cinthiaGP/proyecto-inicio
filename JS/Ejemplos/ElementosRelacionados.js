console.log(document.body.children.length);
console.log(document.body.children);
console.log(document.body.parentElement);

// document.body.children.length;
// document.body.children;
// document.body.parentElement;

var app = document.querySelector('#app');

console.log(app.children);
console.log(app.firstElementChild);
console.log(app.lastElementChild);

// app.children;
// app.firstElementChild;
// app.lastElementChild;

var a = app.querySelector('a');

console.log(a.previousElementSibling);
console.log(a.nextElementSibling);

// a.previousElementSibling;
// a.nextElementSibling;