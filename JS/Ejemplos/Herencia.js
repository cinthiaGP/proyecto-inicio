/*class Forma {
    constructor(){
        console.log('Soy una forma geometrica.');
    }

    gritar(){
        console.log('YEP!');
    }
}

//Clases hijas
class Cuadrado extends Forma {
    constructor(){ //contructor de la clase hija
        super(); //Hace el llamado al constructor padre
        console.log('Soy un cuadrado.');
    }
}

class Circulo extends Forma {
    constructor(){
        super();
        console.log('Soy un circulo.');
    }
}

class Triangulo extends Forma {
    constructor(){
        super();
        console.log('Soy un triangulo.');
    }
}

var c1 = new Cuadrado();
c1.gritar();

var t1 = new Triangulo();
t1.gritar();*/

class Padre {
    tarea(){
        console.log('Tarea del Padre..');
    }

    tarea(value){
        console.log('Tarea del Padre con valor [ sobrecarga ]');
    }
}

(new Padre()).tarea(); //Instancia
//(new Padre()).tarea(quique); 
//En teoria de la sobrecarga, si pasas el parametro como lo marca el metodo pero JS no hace este procedimiento

class Hijo extends Padre {
    tarea(){
        super.tarea(); //Hace el método del padre
        console.log('+ Tarea del Hijo +'); //Hace el método propio 
    }
}

(new Hijo()).tarea();