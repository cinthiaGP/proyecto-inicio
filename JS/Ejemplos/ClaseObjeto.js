/*class Animal {
    constructor(){
        console.warn("Ha nacido un pato.");//warn como alerta [^]
    }

    hablar(){
        return "Cuak";
    }
}

var pato = new Animal();
pato.hablar();
console.log(pato.hablar());

var donald = new Animal();
donald.hablar();*/

// -----------------

class Clase{
    constructor(){
        // var name = 'Luis';
        this.name = 'Luis'; //This apunta a toda la clase
        console.log('Constructor: '+this.name);
    }

    metodo(){
        console.log('Metodo: '+this.name);
    }
}

var c = new Clase();
c.name;
c.metodo();