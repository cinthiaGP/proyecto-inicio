class Forma {
    dibujar(){
        console.log('Dibujando forma..');
    }

    borrar(){
        console.log('Borrando forma..');
    }
}

class Circulo extends Forma {
    dibujar(){
        console.log('Calculando diametro..');
        console.log('Dibujando circulo..');
        this.hacerOtraCosa(); //THIS para que lo busque en la clase Circulo
    }

    borrar(){
        console.log('Borrando circulo..');
    }

    hacerOtraCosa(){
        console.log('Haciendo otras cositas.. shhh')
    }
}

class Cuadrado extends Forma {
    dibujar(){
        console.log('Dibujando lado A..');
        console.log('Dibujando lado B..');
        console.log('Dibujando lado C..');
        console.log('Dibujando lado D..');
    }

    borrar(){
        console.log('Borrando cuadrado..');
    }
}

class Pantalla {
    mostrarFormas(forma){
        forma.dibujar();
    }

    limpiarPantalla(forma){
        forma.borrar();
    }
}

let circulo1 = new Circulo();
let circulo2 = new Circulo();
let cuadrado1 = new Cuadrado();
let myPantalla = new Pantalla();

myPantalla.mostrarFormas(circulo1);
myPantalla.limpiarPantalla(circulo1);

myPantalla.mostrarFormas(circulo2);
myPantalla.limpiarPantalla(circulo2);

myPantalla.mostrarFormas(cuadrado1);
myPantalla.limpiarPantalla(cuadrado1);
