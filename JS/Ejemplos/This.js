//--    EJEMPLO 1
/*let persona = {
    nombre:'Luis',
    edad: 25,
    hablar: function(){ 
        console.log(this.nombre); 
    }
};
persona.hablar();*/

//--    EJEMPLO 2
let decirNombre = function(obj){
    obj.hablar = function(){
        console.log(this.nombre);
    };
};

const BETO = {
    nombre: 'Beto',
    edad: 22
};

const JUAN = {
    nombre: 'Juan',
    edad: 25
};

decirNombre(JUAN);
decirNombre(BETO);

JUAN.hablar();
BETO.hablar();
