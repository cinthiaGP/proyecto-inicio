var miImage = document.querySelector('img');

miImage.onclick = function () {
    var miSrc = miImage.getAttribute('src');
    if (miSrc === 'images/book.png') {
      miImage.setAttribute('src','images/book2.png');
    } else {
      miImage.setAttribute('src', 'images/book.png');
    }
}

var miBoton = document.querySelector('button');
var miTitulo = document.querySelector( 'h2');

function estableceNombreUsuario() {
    var miNombre = prompt('Por favor, ingresa tu nombre.');
    localStorage.setItem('nombre', miNombre);
    miTitulo.textContent = 'Juntos soñando, ' + miNombre;
}
if (!localStorage.getItem('nombre')) {
    estableceNombreUsuario();
}
else {
    var nombreAlmacenado = localStorage.getItem('nombre');
    miTitulo.textContent = 'Juntos soñando, ' + nombreAlmacenado;
}

miBoton.onclick = function() {
    estableceNombreUsuario();
}