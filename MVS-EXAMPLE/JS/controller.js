MVC.Controller = class Controller{
    constructor(props){
        //model es el que envia la petición
        this.eventHandler();

        this.model = new props.model(props.endpoint);
        this.view = new props.vie(props.contentElement);
    }

    eventHandler(){
        document.body.addEventListener('onloadApp', (event) => {
            this.getData();
        });
    }

    getData(){
        this.model.getPersona()
        .then(data => {
            this.view.notify(data);
        })
        .catch(console.log);
    }
}